//
//  CommonLib.swift
//  CommonLib
//

import UIKit

public enum BundleSourceType {
    case other(String)
    case mainApp
}

public class ImageLoader {
    
    public static func load(source: BundleSourceType, name: String, with: UIImage.Configuration? = nil) -> UIImage {
        switch source {
        case .other(let identifier):
            guard let bundlePath = Bundle.main.path(forResource: identifier, ofType: "bundle") else { return UIImage(named: "no_image")! }
            let bundle = Bundle.init(path: bundlePath)
            guard let image =  UIImage(named: name, in: bundle, with: with) else { return UIImage(named: "no_image")! }
            return image
        default:
            guard let image =  UIImage(named: name, in: Bundle.main, with: with) else { return UIImage(named: "no_image")! }
            return image
        }
    }
}


public class StringLoader {
    
    public static func load(forKey key: String) -> String {
        var result = OwnedBundle.bundle.localizedString(forKey: key, value: nil, table: nil)

        if result == key {
            result = OwnedBundle.bundle.localizedString(forKey: key, value: nil, table: OwnedBundle.fallback)
        }

        return result
    }
        
    private class OwnedBundle: NSObject {
        static var bundle: Bundle {
            return Bundle.main
        }
        
        static var fallback: String {
            return "Default<##><##>"
        }
    }
}

public protocol StringProviding: class {
    static var RIBName: String { get }
}

open class StringProvider: StringProviding {
    
    public static var RIBName: String {

        var resultingClassName =  String(describing: self)
        
        if resultingClassName.contains("Strings") {
            resultingClassName = resultingClassName.replacingOccurrences(of: "Strings", with: "")
        }

        return resultingClassName
    }

}

public extension UIApplication {
    var topMostController: UIViewController? {
        let keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
        
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            return topController
        }

        return nil;
    }
}

public func isBackspace(_ string: String) -> Bool {
    
    if let char = string.cString(using: String.Encoding.utf8) {
        let isBackSpace = strcmp(char, "\\b")
        return (isBackSpace == -92)
    }
    
    return false
}

// Source: https://www.tutorialspoint.com/check-if-string-contains-special-characters-in-swift
public extension String {

    func matches(regex: String) -> Bool {
        let testString = NSPredicate(format:"SELF MATCHES %@", regex)
        return testString.evaluate(with: self)
    }

    func applyMask(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.UTF16View.Index(utf16Offset: index, in: self)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    private func indexOf(char: Character) -> Int? {
        return firstIndex(of: char)?.utf16Offset(in: self)
    }
}

public enum ErrorReason: Int, Error {
    
    case unAuthorized = 401
    case userNotFound = 404
    case malformedRequest = 1025
    case unknown = 1026
    case resourceNotFound = 1027
    case imageUploadFailed = 1028
    case imageToDataConversionFailed = 1029
    case dataToImageConversionFailed = 1030
    case verificationCodeAbsent = 1031
    case sameUserName = 1032
    
    public var readable: String {
        switch self {
        case .sameUserName:
            return "No change in username"
        default:
            return ""
        }
    }
}

public typealias ResourceId = String

public enum ImageCompression {
    case compress(value: Float)
    case none
}

public enum ImageSize {
    case large
    case thumbnail
    case regular
}
