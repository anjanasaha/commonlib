//
//  CommonLib.h
//  CommonLib
//
//  Created by Deborshi Saha on 7/30/20.
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for CommonLib.
FOUNDATION_EXPORT double CommonLibVersionNumber;

//! Project version string for CommonLib.
FOUNDATION_EXPORT const unsigned char CommonLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommonLib/PublicHeader.h>


